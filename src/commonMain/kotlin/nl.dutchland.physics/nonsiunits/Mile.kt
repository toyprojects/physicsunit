package nl.dutchland.physics.nonsiunits

import nl.dutchland.physics.baseunits.length.Length

object Mile : Length.Unit by Length.ParameterizedUnit(
        longName = "Mile",
        shortName = "",
        toMeterFactor = 1600.0)