package nl.dutchland.physics.nonsiunits

import nl.dutchland.physics.baseunits.length.Length
import nl.dutchland.physics.baseunits.length.Meter
import nl.dutchland.physics.baseunits.time.Period
import nl.dutchland.physics.baseunits.time.Second
import nl.dutchland.physics.derivedunits.kinematic.speed.Speed

object LightSecond : Length.Unit by Length.Unit.ofParameterized(
        longName = "Light second",
        shortName = "ls",
        toMeterFactor = (Speed.LIGHT_SPEED_THROUGH_VACUUM * Period.of(1.0, Second)).valueIn(Meter))