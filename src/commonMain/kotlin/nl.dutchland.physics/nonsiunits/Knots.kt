package nl.dutchland.physics.nonsiunits

import nl.dutchland.physics.baseunits.time.Hour
import nl.dutchland.physics.derivedunits.kinematic.speed.Speed

object Knots : Speed.Unit by NauticalMile / Hour {
    override val shortName = "kn"
    override val longName = "Knots"
}