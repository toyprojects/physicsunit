package nl.dutchland.physics.nonsiunits

import nl.dutchland.physics.baseunits.time.Second
import nl.dutchland.physics.baseunits.time.Time
import nl.dutchland.physics.derivedunits.energy.EnergyAmount
import nl.dutchland.physics.derivedunits.energy.Joule

private const val PLANCK_CONSTANT: Double = 6.626070040 * 10e-34

class PlanckConstant {
    operator fun div(timePeriod: Time): EnergyAmount {
        return EnergyAmount.of(PLANCK_CONSTANT / timePeriod.valueIn(Second), Joule)
    }
}