package nl.dutchland.physics.nonsiunits

import nl.dutchland.physics.baseunits.length.Length

object NauticalMile : Length.Unit by Length.Unit.ofParameterized(
        longName = "Nautical mile",
        shortName = "NM",
        toMeterFactor = 1852.0)