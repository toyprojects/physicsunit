package nl.dutchland.physics.nonsiunits

import nl.dutchland.physics.baseunits.length.Kilometer
import nl.dutchland.physics.baseunits.time.Hour
import nl.dutchland.physics.derivedunits.kinematic.speed.Speed

object KilometerPerHour : Speed.Unit by Kilometer / Hour