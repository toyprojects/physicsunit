package nl.dutchland.physics

data class RelativeHumidity(val relativeHumidity : Fraction)