package nl.dutchland.physics.baseunits.length

import nl.dutchland.physics.StandardUnitPrefix.*

typealias dm = Decimeter
object Decimeter : Length.Unit by Deci * Meter