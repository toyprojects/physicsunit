package nl.dutchland.physics.baseunits.length

import nl.dutchland.physics.StandardUnitPrefix.*

typealias µm = Nanometer
object Nanometer : Length.Unit by Nano * Meter