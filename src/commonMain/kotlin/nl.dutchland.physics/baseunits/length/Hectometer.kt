package nl.dutchland.physics.baseunits.length

import nl.dutchland.physics.StandardUnitPrefix.*

typealias hm = Hectometer
object Hectometer : Length.Unit by Hecto * Meter