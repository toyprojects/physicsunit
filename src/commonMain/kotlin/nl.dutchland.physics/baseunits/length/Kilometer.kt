package nl.dutchland.physics.baseunits.length

import nl.dutchland.physics.StandardUnitPrefix.*

typealias km = Kilometer
object Kilometer : Length.Unit by Kilo * Meter