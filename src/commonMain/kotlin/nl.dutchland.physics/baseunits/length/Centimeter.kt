package nl.dutchland.physics.baseunits.length

import nl.dutchland.physics.StandardUnitPrefix.Centi

typealias cm = Centimeter
object Centimeter : Length.Unit by Centi * Meter