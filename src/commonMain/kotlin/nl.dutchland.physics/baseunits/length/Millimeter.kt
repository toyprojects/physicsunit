package nl.dutchland.physics.baseunits.length

import nl.dutchland.physics.StandardUnitPrefix.*

typealias mm = Millimeter
object Millimeter : Length.Unit by Milli * Meter