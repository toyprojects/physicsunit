package nl.dutchland.physics.baseunits.temperature

object Celsius : Temperature.Scale() {
    internal const val absoluteZero = -273.15

    override val longName = "Celsius"
    override val shortName = "°C"

    override
    fun fromKelvin(valueInKelvin: Double): Double =
            valueInKelvin + absoluteZero

    override
    fun toKelvin(value: Double): Double =
            value - absoluteZero
}