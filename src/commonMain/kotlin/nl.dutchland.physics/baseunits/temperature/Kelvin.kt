package nl.dutchland.physics.baseunits.temperature

object Kelvin : Temperature.Scale() {
    internal const val absoluteZero = 0.0
    override val longName = "Kelvin"
    override val shortName = "K"

    override fun fromKelvin(valueInKelvin: Double): Double = valueInKelvin
    override fun toKelvin(value: Double): Double = value
}
