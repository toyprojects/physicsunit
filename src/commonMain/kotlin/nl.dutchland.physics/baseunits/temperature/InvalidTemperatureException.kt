package nl.dutchland.physics.baseunits.temperature

class InvalidTemperatureException(message: String) : IllegalArgumentException(message)