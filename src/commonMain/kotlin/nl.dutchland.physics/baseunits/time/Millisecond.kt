package nl.dutchland.physics.baseunits.time

import nl.dutchland.physics.StandardUnitPrefix.Milli

typealias ms = Millisecond
object Millisecond : Time.Unit by Milli * Second


