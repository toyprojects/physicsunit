package nl.dutchland.physics.baseunits.time

typealias h = Hour
object Hour : Time.Unit by FactorizedUnit(
        toSecondFactor = 60.0 * 60.0,
        shortName = "h",
        longName = "Hour")