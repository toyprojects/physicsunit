package nl.dutchland.physics.baseunits.time

typealias s = Second
object Second : Time.Unit by FactorizedUnit(
        toSecondFactor = 1.0,
        shortName = "s",
        longName = "Second")