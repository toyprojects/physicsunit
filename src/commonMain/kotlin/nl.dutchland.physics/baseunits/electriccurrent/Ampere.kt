package nl.dutchland.physics.baseunits.electriccurrent

typealias A = Ampere
object Ampere : ElectricCurrent.Unit by ElectricCurrent.Unit.ofParameterized(
        "Ampere",
        "A",
        1.0)