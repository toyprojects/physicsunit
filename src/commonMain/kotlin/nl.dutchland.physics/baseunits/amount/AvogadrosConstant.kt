package nl.dutchland.physics.baseunits.amount

object AvogadrosConstant {
    val particlesPerMol = 6_022_140_857 * 10e23
}