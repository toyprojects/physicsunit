package nl.dutchland.physics.derivedunits.specificheatcapacity

import nl.dutchland.physics.baseunits.mass.Mass
import nl.dutchland.physics.derivedunits.heatcapacity.HeatCapacity

class SpecificHeatCapacity private constructor(
        private val valueInJoulePerKilogramPerKelvin: Double) {

    companion object {
        fun of(value: Double, unit: SpecificHeatCapacity.Unit) : SpecificHeatCapacity {
            TODO()
        }
    }

    fun valueIn(unit: SpecificHeatCapacity.Unit): Double {
        TODO()
    }

    operator fun times(mass: Mass) : HeatCapacity {
        TODO()
    }


    interface Unit {
        fun fromJoulePerKelvinPerKilogram(valueInJoulePerKelvin: Double) : Double
        fun toJoulePerKelvinPerKilogram(value: Double) : Double
    }
}