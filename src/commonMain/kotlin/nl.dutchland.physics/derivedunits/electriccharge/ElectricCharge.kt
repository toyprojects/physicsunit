package nl.dutchland.physics.derivedunits.electriccharge

class ElectricCharge(private val value: Double,
                     private val unit: Unit) {

    companion object {
        fun of(value: Double, unit: Unit): ElectricCharge {
            return ElectricCharge(value, unit)
        }
    }


    class Unit
}