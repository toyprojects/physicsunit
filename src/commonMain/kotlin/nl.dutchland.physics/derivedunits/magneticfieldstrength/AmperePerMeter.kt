package nl.dutchland.physics.derivedunits.magneticfieldstrength

import nl.dutchland.physics.baseunits.electriccurrent.Ampere
import nl.dutchland.physics.baseunits.length.Meter

object AmperePerMeter : MagneticFieldStrength.Unit by Ampere / Meter