package nl.dutchland.physics.derivedunits.energy

import nl.dutchland.physics.derivedunits.mechanical.area.m2
import nl.dutchland.physics.baseunits.mass.Kilogram
import nl.dutchland.physics.baseunits.time.Second

object Joule : EnergyAmount.Unit by EnergyAmount.Unit.ofParameterized(m2, Kilogram, Second) {
    override val shortName: String = "J"
    override val longName: String = "Joule"

    override fun fromJoule(valueInJoule: Double): Double = valueInJoule
    override fun toJoule(value: Double): Double = value
}