package nl.dutchland.physics.derivedunits.frequency

class Hertz : Frequency.Unit by Frequency.FactorizedUnit(
        "Hertz",
        "Hz",
        1.0)