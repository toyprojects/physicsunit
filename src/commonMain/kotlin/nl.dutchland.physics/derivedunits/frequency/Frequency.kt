package nl.dutchland.physics.derivedunits.frequency

import nl.dutchland.physics.baseunits.time.Second
import nl.dutchland.physics.baseunits.time.Time

class Frequency internal constructor(private val valueInHertz: Double) {

    companion object {
        fun of(value: Double, unit: Unit): Frequency {
            TODO()
        }
    }

    fun valueIn(unit: Unit) {
        TODO()
    }

    operator fun times(time: Time) : Double {
        return valueInHertz * time.valueIn(Second)
    }

    interface Unit {
        fun toHertz(value: Double): Double
        fun fromHertz(valueInHertz: Double): Double

        val longName: String
        val shortName: String

        override fun toString(): String
    }

    class FactorizedUnit(override val longName: String,
                         override val shortName: String,
                         private val toHertzFactor: Double) : Unit {
        override fun toHertz(value: Double) = value * toHertzFactor
        override fun fromHertz(valueInHertz: Double) = valueInHertz / toHertzFactor

        override fun toString(): String = longName
    }
}