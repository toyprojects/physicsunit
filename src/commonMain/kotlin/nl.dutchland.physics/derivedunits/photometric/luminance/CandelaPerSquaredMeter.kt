package nl.dutchland.physics.derivedunits.photometric.luminance

import nl.dutchland.physics.baseunits.luminousintensity.Candela
import nl.dutchland.physics.derivedunits.mechanical.area.m2

class CandelaPerSquaredMeter : Luminance.Unit by Candela / m2 {
}