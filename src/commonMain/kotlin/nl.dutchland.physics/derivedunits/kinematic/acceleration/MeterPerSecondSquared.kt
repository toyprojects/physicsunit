package nl.dutchland.physics.derivedunits.kinematic.acceleration

import nl.dutchland.physics.baseunits.length.Meter
import nl.dutchland.physics.baseunits.time.Second

object MeterPerSecondSquared : Acceleration.Unit by (Meter / Second) / Second {
    override val longName: String = "Meter per Second squared"
    override val shortName: String = "m/s^2"
}