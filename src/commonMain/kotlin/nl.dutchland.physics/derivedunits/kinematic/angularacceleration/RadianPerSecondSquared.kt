package nl.dutchland.physics.derivedunits.kinematic.angularacceleration

import nl.dutchland.physics.baseunits.time.Second
import nl.dutchland.physics.derivedunits.angle.Radian

class RadianPerSecondSquared : AngularAcceleration.Unit by (Radian / Second) / Second {
    override val longName: String = "Radian per Second squared"
    override val shortName: String = "rad/s^2"
}