package nl.dutchland.physics.derivedunits.kinematic.speed

import nl.dutchland.physics.baseunits.length.Meter
import nl.dutchland.physics.baseunits.time.Second

object MeterPerSecond : Speed.Unit by Meter / Second