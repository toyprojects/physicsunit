package nl.dutchland.physics.derivedunits.kinematic.angularvelocity

import nl.dutchland.physics.baseunits.time.Second
import nl.dutchland.physics.derivedunits.angle.Radian

object RadianPerSecond : AngularVelocity.Unit by Radian / Second