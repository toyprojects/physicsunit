package nl.dutchland.physics.derivedunits.amountofsubstanceconcentration

import nl.dutchland.physics.baseunits.amount.AmountOfSubstance
import nl.dutchland.physics.derivedunits.mechanical.volume.Volume

class AmountOfSubstanceConcentration internal constructor(
        private val molPerCubicMeter : Double){

    fun valueIn(unit: AmountOfSubstanceConcentration.Unit) : Double {
        TODO()
    }

    operator fun times(volume : Volume) : AmountOfSubstance {
        TODO()
    }

    operator fun div(amountOfSubstance: AmountOfSubstance) : Volume {
        TODO()
    }

    interface Unit {
        val shortName: String
        val longName: String
    }
}