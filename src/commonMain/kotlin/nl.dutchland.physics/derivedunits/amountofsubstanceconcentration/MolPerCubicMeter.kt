package nl.dutchland.physics.derivedunits.amountofsubstanceconcentration

import nl.dutchland.physics.baseunits.amount.Mol
import nl.dutchland.physics.derivedunits.mechanical.volume.m3

class MolPerCubicMeter : AmountOfSubstanceConcentration.Unit by Mol / m3