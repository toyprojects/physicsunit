package nl.dutchland.physics.derivedunits.power

import nl.dutchland.physics.baseunits.time.Second
import nl.dutchland.physics.derivedunits.energy.Joule

object Watt : Power.Unit by Joule / Second {
    override val shortName: String = "W"
    override val longName: String = "Watt"
}