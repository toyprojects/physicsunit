package nl.dutchland.physics.derivedunits.pressure

import nl.dutchland.physics.derivedunits.mechanical.area.m2
import nl.dutchland.physics.derivedunits.force.N

class Pascal : Pressure.Unit by N / m2 {
    override val shortName: String = "Pa"
    override val longName: String = "Pascal"
}