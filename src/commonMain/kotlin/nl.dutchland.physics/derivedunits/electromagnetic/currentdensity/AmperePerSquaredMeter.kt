package nl.dutchland.physics.derivedunits.electromagnetic.currentdensity

import nl.dutchland.physics.baseunits.electriccurrent.A
import nl.dutchland.physics.derivedunits.mechanical.area.m2

object AmperePerSquaredMeter : CurrentDensity.Unit by A / m2